# react-vite-vim-friendly-template

A React template built with Vite that works nicely with [Vim][vim].

### What's inside?
* [React][react]
* [Vite][vite]
* [ESLint][eslint]
* [Prettier][prettier]

### Getting started

```bash
# Download repo via:
npx tiged gitlab/ian-s-mcb/react-vite-vim-friendly-template my-app
# OR
npx tiged git@gitlab.com:ian-s-mcb/react-vite-vim-friendly-template my-app

# Start project
cd my-app
git init
npm i
npm run dev
```

### Vim config
Install [ALE][ale] and add the following to your .vimrc.
```vimscript
" Ale
let g:ale_linters = {
\   'javascript': ['eslint'],
\   'javascriptreact': ['eslint'],
\   'typescript': ['eslint'],
\   'typescriptreact': ['eslint'],
\}
let g:ale_fixers = {
\   'html': ['prettier'],
\   'javascript': ['prettier'],
\   'javascriptreact': ['prettier'],
\   'typescript': ['prettier'],
\   'typescriptreact': ['prettier'],
\   'css': ['prettier'],
\}
let g:ale_fix_on_save = 1
let g:ale_linters_explicit = 1
nmap <silent> <leader>d <Plug>(ale_detail)
nmap <silent> <leader>j <Plug>(ale_next_wrap)
nmap <silent> <leader>k <Plug>(ale_previous_wrap)
nmap <silent> <leader>f <Plug>(ale_fix)
nmap <silent> <leader>l <Plug>(ale_lint)
```

[ale]: https://github.com/dense-analysis/ale
[eslint]: https://eslint.org
[prettier]: https://prettier.io/
[react]: https://reactjs.org
[vim]: https://en.wikipedia.org/wiki/Vim_(text_editor)
[vite]: https://vitejs.dev
